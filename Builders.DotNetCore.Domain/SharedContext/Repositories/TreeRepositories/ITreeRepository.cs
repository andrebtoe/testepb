﻿using Builders.DotNetCore.Domain.SharedContext.Entities;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Repositories.TreeRepositories
{
    public interface ITreeRepository
    {
        bool Add(Tree tree);
        bool AddChildren(Tree tree);
        IList<Tree> GetAll();
        Tree GetByTreeId(string treeId);
    }
}