﻿using FluentValidation.Results;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.Outputs
{
    public class CommandResult<TQueryResult> : ICommandResult<TQueryResult>
    where TQueryResult : ICommandOutputResult
    {
        private readonly IList<ValidationFailure> _errors;

        public CommandResult() { }

        public CommandResult(bool success, string message, ValidationFailure validationFailure)
        {
            Success = success;
            Message = message;
            _errors = new List<ValidationFailure>() { validationFailure };
        }

        public CommandResult(bool success, string message, TQueryResult result, IList<ValidationFailure> errors)
        {
            Success = success;
            Message = message;
            Result = result;
            _errors = errors;
        }

        public CommandResult(bool success, string message, IList<ValidationFailure> errors)
        {
            Success = success;
            Message = message;
            _errors = errors;
        }

        public bool Success { get; set; }
        public string Message { get; set; }
        public TQueryResult Result { get; set; }
    }
}