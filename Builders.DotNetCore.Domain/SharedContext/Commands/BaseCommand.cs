﻿using Builders.DotNetCore.Domain.SharedContext.Specifications;
using FluentValidation;

namespace Builders.DotNetCore.Domain.SharedContext.Commands
{
    public abstract class BaseCommand<TCommandSpecification> : BaseValidationSpecification<TCommandSpecification>
        where TCommandSpecification : IValidator, new()
    { }
}