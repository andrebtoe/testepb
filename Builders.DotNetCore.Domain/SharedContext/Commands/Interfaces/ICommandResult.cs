﻿namespace Builders.DotNetCore.Domain.SharedContext.Commands
{
    public interface ICommandResult
    {
        bool Success { get; set; }
    }
}