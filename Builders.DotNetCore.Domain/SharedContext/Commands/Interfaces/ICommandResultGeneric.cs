﻿namespace Builders.DotNetCore.Domain.SharedContext.Commands
{
    public interface ICommandResult<TQueryResult> : ICommandResult
    where TQueryResult : ICommandOutputResult
    {
        string Message { get; set; }
        TQueryResult Result { get; set; }
    }
}