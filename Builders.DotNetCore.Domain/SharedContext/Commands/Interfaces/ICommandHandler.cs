﻿namespace Builders.DotNetCore.Domain.SharedContext.Commands
{
    public interface ICommandHandler<TQueryResult, TCommand>
    where TQueryResult : ICommandOutputResult
    where TCommand : ICommand
    {
        ICommandResult<TQueryResult> Handle(TCommand command);
    }
}