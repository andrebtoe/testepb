﻿using FluentValidation.Results;

namespace Builders.DotNetCore.Domain.SharedContext.Commands
{
    public interface ICommand
    {
        ValidationResult Validate();
    }
}