﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs.Specifications;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs
{
    public class CreateTreeCommand : BaseCommand<CreateTreeCommandSpecification>, ICommand
    {
        public string Id { get; set; }
        public int Value { get; set; }
        public IList<CreateTreeChildCommand> Children { get; set; } = new List<CreateTreeChildCommand>();
    }
}