﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs.Specifications;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs
{
    public class UpdateTreeCommand : BaseCommand<UpdateTreeCommandSpecification>, ICommand
    {
        public string TreeId { get; set; }
        public int Value { get; set; }
        public IList<CreateTreeChildCommand> Children { get; set; } = new List<CreateTreeChildCommand>();
    }
}