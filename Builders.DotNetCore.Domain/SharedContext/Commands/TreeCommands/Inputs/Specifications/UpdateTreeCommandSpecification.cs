﻿using FluentValidation;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs.Specifications
{
    public class UpdateTreeCommandSpecification : AbstractValidator<UpdateTreeCommand>
    {
        public UpdateTreeCommandSpecification()
        {
            RuleFor(x => x.TreeId).NotEmpty();
            RuleFor(x => x.Value).GreaterThan(0);
            RuleFor(x => x.Children).NotNull();
            RuleFor(x => x.Children.Count).GreaterThan(0).When(x => x.Children != null);
        }
    }
}