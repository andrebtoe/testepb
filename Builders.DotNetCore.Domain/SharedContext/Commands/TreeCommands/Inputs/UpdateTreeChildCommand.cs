﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs.Specifications;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs
{
    public class UpdateTreeChildCommand : BaseCommand<UpdateTreeChildCommandSpecification>
    {
        public int Value { get; set; }
        public IList<UpdateTreeChildCommand> Children { get; set; } = new List<UpdateTreeChildCommand>();
    }
}