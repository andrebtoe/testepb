﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs.Specifications;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs
{
    public class CreateTreeChildCommand : BaseCommand<CreateTreeChildCommandSpecification>
    {
        public int Value { get; set; }
        public IList<CreateTreeChildCommand> Children { get; set; } = new List<CreateTreeChildCommand>();
    }
}