﻿using Builders.DotNetCore.Domain.SharedContext.Entities.Specifications;
using System;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Entities
{
    public class TreeChild : BaseEntity<TreeChildSpecification>
    {
        public TreeChild() { }

        public TreeChild(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
        public List<TreeChild> Children { get; set; } = new List<TreeChild>();

        public void AddTreeChild(TreeChild treeChild)
        {
            if (treeChild == null)
                throw new ArgumentException("O argumento treeChild não pode ser null");

            Children.Add(treeChild);
        }
    }
}