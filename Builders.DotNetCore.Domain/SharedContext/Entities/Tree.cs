﻿using Builders.DotNetCore.Domain.SharedContext.Entities.Specifications;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Entities
{
    public class Tree : BaseEntity<TreeSpecification>
    {
        public Tree() { }

        public Tree(int value)
        {
            Value = value;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public int Value { get; set; }
        public IList<TreeChild> Children { get; set; } = new List<TreeChild>();

        public void AddTreeChild(TreeChild treeChild)
        {
            if (treeChild == null)
                throw new ArgumentException("O argumento treeChild não pode ser null");

            Children.Add(treeChild);
        }
    }
}