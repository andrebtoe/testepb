﻿using Builders.DotNetCore.Domain.SharedContext.Specifications;
using FluentValidation;

namespace Builders.DotNetCore.Domain.SharedContext.Entities
{
    public abstract class BaseEntity<TSpecification> : BaseValidationSpecification<TSpecification>
        where TSpecification : IValidator, new()
    { }
}