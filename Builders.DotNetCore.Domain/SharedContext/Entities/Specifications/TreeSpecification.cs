﻿using FluentValidation;

namespace Builders.DotNetCore.Domain.SharedContext.Entities.Specifications
{
    public class TreeSpecification : AbstractValidator<Tree>
    {
        public TreeSpecification()
        {
            RuleFor(x => x.Value).GreaterThan(0);
            RuleFor(x => x.Children).NotNull();
            RuleFor(x => x.Children.Count).GreaterThan(0).When(x => x.Children != null);
        }
    }
}