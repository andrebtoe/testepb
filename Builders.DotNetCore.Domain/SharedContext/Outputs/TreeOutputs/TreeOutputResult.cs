﻿using Builders.DotNetCore.Domain.SharedContext.Commands;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Outputs.TreeOutputs
{
    public class TreeOutputResult : ICommandOutputResult
    {
        public string Id { get; set; }
        public int Value { get; set; }
        public IList<TreeChildOutputResult> Children { get; set; } = new List<TreeChildOutputResult>();
    }
}