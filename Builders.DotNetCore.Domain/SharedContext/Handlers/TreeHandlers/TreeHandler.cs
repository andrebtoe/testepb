﻿using Builders.DotNetCore.Domain.SharedContext.Commands;
using Builders.DotNetCore.Domain.SharedContext.Commands.Outputs;
using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs;
using Builders.DotNetCore.Domain.SharedContext.Entities;
using Builders.DotNetCore.Domain.SharedContext.Handlers.TreeHandlers.Interfaces;
using Builders.DotNetCore.Domain.SharedContext.Outputs.TreeOutputs;
using Builders.DotNetCore.Domain.SharedContext.Repositories.TreeRepositories;
using Builders.DotNetCore.Domain.SharedContext.Specifications;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Handlers.TreeHandlers
{
    public class TreeHandler : BaseValidation, ITreeHandler
    {
        private readonly ITreeRepository _treeRepository;

        public TreeHandler(ITreeRepository treeRepository)
        {
            _treeRepository = treeRepository;
        }

        public ICommandResult<TreeOutputResult> Handle(CreateTreeCommand createTreeCommand)
        {
            // Criar entidade
            var tree = new Tree(createTreeCommand.Value);

            // Atribuir
            FillChildrenByReference(createTreeCommand.Children, tree);

            // Validar entidades
            AddErrors(tree.Errors);

            if (!IsValid)
                return new CommandResult<TreeOutputResult>(false, null, Errors);

            // Persistir
            var persisted = _treeRepository.Add(tree);

            if (!persisted)
                AddError("", "Erro ao tentar cadastrar");

            var treeOutputResult = TreeToTreeOutputResultMap(tree);

            return new CommandResult<TreeOutputResult>(true, null, treeOutputResult, null);
        }

        public ICommandResult<TreeOutputResult> Handle(UpdateTreeCommand updateTreeCommand)
        {
            // Obter entidade
            var tree = _treeRepository.GetByTreeId(updateTreeCommand.TreeId);
            if (tree == null)
                AddError("", "TreeId não encontrada");

            if (!IsValid)
                return new CommandResult<TreeOutputResult>(false, null, Errors);

            // Adicinar no
            FillChildrenByReference(updateTreeCommand.Children, tree, null);

            // Persistir
            var persisted = _treeRepository.AddChildren(tree);

            if (!persisted)
                AddError("", "Erro ao tentar cadastrar");

            var treeOutputResult = TreeToTreeOutputResultMap(tree);

            return new CommandResult<TreeOutputResult>(true, null, treeOutputResult, null);
        }

        public void FillChildrenByReference(IList<CreateTreeChildCommand> children, Tree tree = null, TreeChild treeChild = null)
        {
            if (children == null)
                return;

            foreach (var childrenItemCommmand in children)
            {
                var treeChildNewLevel = new TreeChild(childrenItemCommmand.Value);
                if (tree != null)
                    tree.AddTreeChild(treeChildNewLevel);
                else
                    treeChild.AddTreeChild(treeChildNewLevel);

                if (childrenItemCommmand.Children != null)
                    FillChildrenByReference(childrenItemCommmand.Children, null, treeChildNewLevel);
            }
        }

        public TreeOutputResult TreeToTreeOutputResultMap(Tree tree)
        {
            var treeOutputResult = new TreeOutputResult()
            {
                Id = tree.Id,
                Value = tree.Value
            };

            AddChildrenByReference(tree.Children, treeOutputResult, null);

            return treeOutputResult;
        }

        private void AddChildrenByReference(IList<TreeChild> children, TreeOutputResult treeOutputResult = null, TreeChildOutputResult treeChildOutputResult = null)
        {
            if (children == null)
                return;

            foreach (var childrenItem in children)
            {
                var treeChildOutputResultNew = new TreeChildOutputResult()
                {
                    Value = childrenItem.Value
                };

                if (treeOutputResult != null)
                    treeOutputResult.Children.Add(treeChildOutputResultNew);
                else
                    treeChildOutputResult.Children.Add(treeChildOutputResultNew);

                if (childrenItem.Children != null)
                    AddChildrenByReference(childrenItem.Children, treeOutputResult, treeChildOutputResult);
            }
        }
    }
}