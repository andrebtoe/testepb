﻿using Builders.DotNetCore.Domain.SharedContext.Commands;
using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs;
using Builders.DotNetCore.Domain.SharedContext.Outputs.TreeOutputs;

namespace Builders.DotNetCore.Domain.SharedContext.Handlers.TreeHandlers.Interfaces
{
    public interface ITreeHandler : ICommandHandler<TreeOutputResult, CreateTreeCommand>, ICommandHandler<TreeOutputResult, UpdateTreeCommand> { }
}