﻿using FluentValidation.Results;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Specifications
{
    public interface IBaseValidation
    {
        IList<ValidationFailure> Errors { get; }
    }
}