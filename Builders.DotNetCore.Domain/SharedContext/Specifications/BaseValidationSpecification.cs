﻿using FluentValidation;
using FluentValidation.Results;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Specifications
{
    public abstract class BaseValidationSpecification<TValidationSpecification>
        where TValidationSpecification : IValidator, new()
    {
        public ValidationResult Validate()
        {
            var validation = new TValidationSpecification();
            var validationResult = validation.Validate(this);
            return validationResult;
        }

        [BsonIgnore]
        public IList<ValidationFailure> Errors
        {
            get { return Validate().Errors; }
        }
    }
}