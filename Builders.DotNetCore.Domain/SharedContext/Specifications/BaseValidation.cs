﻿using FluentValidation.Results;
using System.Collections.Generic;

namespace Builders.DotNetCore.Domain.SharedContext.Specifications
{
    public abstract class BaseValidation : IBaseValidation
    {
        private readonly List<ValidationFailure> _errors;

        public BaseValidation()
        {
            _errors = new List<ValidationFailure>();
        }

        public void AddError(string propertyName, string errorMessage)
        {
            _errors.Add(new ValidationFailure(propertyName, errorMessage));
        }

        public void AddErrors(IList<ValidationFailure> errors)
        {
            _errors.AddRange(errors);
        }

        public IList<ValidationFailure> Errors
        {
            get { return _errors; }
        }

        public bool IsValid
        {
            get { return _errors.Count == 0; }
        }
    }
}