﻿using Builders.DotNetCore.Data;
using Builders.DotNetCore.Domain.SharedContext.Entities;
using Builders.DotNetCore.Domain.SharedContext.Repositories.TreeRepositories;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Builders.DotNetCore.Infrastructure.SharedContext.Repositories.TreeRepositories
{
    public class TreeRepository : ITreeRepository
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMongoCollection<Tree> _treeCollection;

        public TreeRepository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _treeCollection = _unitOfWork.Database.GetCollection<Tree>("Andre_Zarlenga_Tree");
        }

        public bool Add(Tree tree)
        {
            try
            {
                _treeCollection.InsertOne(tree);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool AddChildren(Tree tree)
        {
            try
            {
                Expression<Func<Tree, bool>> filterUpdate = x => x.Id.Equals(tree.Id);
                _treeCollection.ReplaceOne(filterUpdate, tree);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public IList<Tree> GetAll()
        {
            var tree = _treeCollection.Find(x => true).ToList();
            return tree;
        }

        public Tree GetByTreeId(string treeId)
        {
            var tree = _treeCollection.Find(x => x.Id == treeId).SingleOrDefault();
            return tree;
        }
    }
}