@Library('scm-builders') _ 
pipeline {
    environment {
        //--- RequestMapping
        requestMappingURI   = "/api"

        //-- Cloud provider [ google , AWS ]
        cloudProvider       = 'google'

        //--- Cloud Disk Repository
        appDiskRepoDev      = "gcr.io/ilabs-205516/${getRepoName()}"
        appDiskRepoHml      = "gcr.io/ilabs-205516/${getRepoName()}"
        appDiskRepoPrd      = "gcr.io/ilabs-205516/${getRepoName()}"

        //--- Secrets Repository - <account/repository.git>
        secretsRepository   = 'platformbuilders/desafiodotnet-secrets.git'

        //--- Cloud Credencial 
        cloudCredencialHml  = 'gcr:credencialhml'
        cloudCredencialPrd  = 'gcr:credencialprd'
        
        //--- K8s Namespaces
        namespacek8sDev     = 'develop'
        namespacek8sHml     = 'homolog'
        namespacek8sPrd     = 'default'

        //--- Notication Vars
        slackChannel        = 'jenkins-iguatemi' 

        // Internal Control
        userInput = false
	}
    agent any
    stages {
        stage('Checkout') {			
            steps {
                script {
                    slackSend (channel: "#${slackChannel}", color: '#74E862', 
                        message: "START Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
                    echo "Checkout ${env.BRANCH_NAME}..."
                    deleteDir()
                    git([url: getRepoUrl(), branch: env.BRANCH_NAME, credentialsId: appCredencial()])
               }
            }
        }
        stage('Build') {		
            steps {
                script {
                    echo 'Building..'
                    sh "docker-compose build webapi"
                    stash includes: '**', name: 'app', useDefaultExcludes: true 
                }
            }
        }
        stage('Unit Test') {
            steps {
                script {
                    echo 'Unit Tests..'
                    sh "docker-compose up --force-recreate tests.unit"
                }    
            }
        }
        stage('Integration Test') {
			when {
                expression{ return (env.BRANCH_NAME == "develop")}
            }
           	steps {
				echo 'Integration Test ....'
                sh "docker-compose up --force-recreate tests.integration"    
           	}
	    }
        stage('Sonar') {
            when {
                expression{ return (env.BRANCH_NAME == "develop")}
            }
            steps {
                echo 'Sonar Validation..'
                //executeSonar()
            }
        }
        stage('Send Alert') {
            when {
                expression { return (env.BRANCH_NAME.find("release"))}
            }
            steps {
                script {
                    if (slackChannel != '' && slackChannel != null ) {  
                        echo 'Send slack notification ...'
                        slackSend (channel: "#${slackChannel}", color: '#FFFF00', 
                            message: "Waiting Confirmation to Deploy: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
                    } else echo 'Slack notification disable...'    

                    sendEmail('$DEFAULT_RECIPIENTS', "approve", 
                        "[JENKINS - RELEASE] Novo job aguardando aprova��o")
                }                 
            }
        }
        stage('Promotion RC?') {
			when {
                expression { return (env.BRANCH_NAME.find("release"))}
            }
			steps {
               script {
					userInput = input(
								id: 'Proceed1', message: 'Was this successful?', parameters: [
								[$class: 'BooleanParameterDefinition', defaultValue: true, description: '', name: 'Confirm deploy?']
								])
               }
            }
        }
        stage('Docker DEV') {
			when {
                expression { return (env.BRANCH_NAME == "develop")}
            }
            steps {

                script{

                    unstash 'app'
                    _host = "${appDiskRepoDev}"
                    _sufix = "-${namespacek8sDev}"
                    _BUILD_NUMBER = env.BUILD_NUMBER
                    _credencial = cloudCredencialHml
                    buildName = _host
                    app = docker.build("${buildName}${_sufix}:${_BUILD_NUMBER}", "-f Builders.DotNetCore.WebApi/Dockerfile .")

                    docker.withRegistry("https://${_host}", _credencial) {
                        app.push("${_BUILD_NUMBER}")
                        app.push("latest")
                    }

                    stash includes: '**', name: 'app'
                }

                //pushImageToRepo("${appDiskRepoDev}",cloudCredencialHml, env.BUILD_NUMBER,"-${namespacek8sDev}",cloudProvider, "-f Builders.DotNetCore.WebApi/Dockerfile .")                             
            }
        }
        stage('Docker HML') {
			when {
                expression { return (env.BRANCH_NAME.find("release")  && userInput == true)}
            }
            steps {

                script{

                    unstash 'app'
                    _host = "${appDiskRepoHml}"
                    _sufix = "-${namespacek8sHml}"
                    _BUILD_NUMBER = env.BUILD_NUMBER
                    _credencial = cloudCredencialHml
                    buildName = _host
                    app = docker.build("${buildName}${_sufix}:${_BUILD_NUMBER}", "-f Builders.DotNetCore.WebApi/Dockerfile .")

                    docker.withRegistry("https://${_host}", _credencial) {
                        app.push("${_BUILD_NUMBER}")
                        app.push("latest")
                    }

                    stash includes: '**', name: 'app'
                }

                //pushImageToRepo("${appDiskRepoHml}",cloudCredencialHml, env.BUILD_NUMBER,"-${namespacek8sHml}",cloudProvider)                             
            }
        }
        stage('Deploy Services') {
			when {
                expression { return ( (env.BRANCH_NAME.find("release") && userInput == true) || env.BRANCH_NAME == "develop" )}
            }
			steps {
				echo 'Services apply ...'
               	script {
                    git([url: getRepoUrl(), branch: env.BRANCH_NAME, credentialsId: appCredencial()])   
                    if(env.BRANCH_NAME == "develop"){   
                        replaceContentFile('branch',env.BRANCH_NAME , 'k8s-service-develop.yaml')
                        replaceContentFile('appName',getRepoName(),'k8s-service-develop.yaml')
                        executeKubectl("k8s-service-develop.yaml",namespacek8sDev,cloudProvider)
                    } 
                    else if(env.BRANCH_NAME.find("release")){
                        replaceContentFile('branch',env.BRANCH_NAME , 'k8s-service-homolog.yaml')
                        replaceContentFile('appName',getRepoName(),'k8s-service-homolog.yaml')
                        executeKubectl("k8s-service-homolog.yaml",namespacek8sHml,cloudProvider)
                    }     
                }
            }
        }
        
        stage('Deploy Secret') {
			when {
                expression { return ( (env.BRANCH_NAME.find("release") && userInput == true) || env.BRANCH_NAME == "develop" )}
            }
			steps {
               	script {
                    git([url: getRepoSecrets(secretsRepository), branch: 'master', credentialsId: appCredencial()])
                    
                    if(env.BRANCH_NAME == "develop"){   
                        if (fileExists("${getRepoName()}/k8s-secret-develop.yaml")) {
                            echo 'Secret apply develop...'
                            replaceContentFile('branch',env.BRANCH_NAME , "${getRepoName()}/k8s-secret-develop.yaml")
                            replaceContentFile('appName',getRepoName(),"${getRepoName()}/k8s-secret-develop.yaml")
                            executeKubectl("${getRepoName()}/k8s-secret-develop.yaml",namespacek8sDev,cloudProvider)
                        }    
                    } 
                    else if(env.BRANCH_NAME.find("release")){
                        if (fileExists("${getRepoName()}/k8s-secret-homolog.yaml")) {
                            echo 'Secret apply homolog...'
                            replaceContentFile('branch',env.BRANCH_NAME , "${getRepoName()}/k8s-secret-homolog.yaml")
                            replaceContentFile('appName',getRepoName(),"${getRepoName()}/k8s-secret-homolog.yaml")
                            executeKubectl("${getRepoName()}/k8s-secret-homolog.yaml",namespacek8sHml,cloudProvider)
                        }    
                    }     
                }
            }
        }
        
        stage('Deploy Image') {
            when {
                 expression { return ( (env.BRANCH_NAME.find("release") && userInput == true) || env.BRANCH_NAME == "develop" )}
            }
            steps {
                unstash 'app'
                script {
                    if (env.BRANCH_NAME == "develop") {
                        echo 'Deploying....Develop'
                        replaceContentFile('branch',env.BRANCH_NAME,'k8s-deployment-develop.yaml')
                        replaceContentFile('version',env.BUILD_NUMBER,'k8s-deployment-develop.yaml')
                        replaceContentFile('appName',getRepoName(),'k8s-deployment-develop.yaml')
                        executeKubectl("k8s-deployment-develop.yaml",namespacek8sDev,cloudProvider)
                    } else if (env.BRANCH_NAME.find("release")) {
                        echo 'Deploying....Homolog'
                        replaceContentFile('branch',env.BRANCH_NAME,'k8s-deployment-homolog.yaml')
                        replaceContentFile('version',env.BUILD_NUMBER,'k8s-deployment-homolog.yaml')
                        replaceContentFile('appName',getRepoName(),'k8s-deployment-homolog.yaml')
                        executeKubectl("k8s-deployment-homolog.yaml",namespacek8sHml,cloudProvider)
                    }
                } 
           }
        }
        stage('Docker PRD') {
			when {
                expression { return (env.BRANCH_NAME.find("release")  && userInput == true)}
            }
            steps {

                 script{

                    unstash 'app'
                    _host = "${appDiskRepappDiskRepoPrdoDev}"
                    _BUILD_NUMBER = env.BUILD_NUMBER
                    _credencial = cloudCredencialPrd
                    buildName = _host
                    app = docker.build("${buildName}:${_BUILD_NUMBER}", "-f Builders.DotNetCore.WebApi/Dockerfile .")

                    docker.withRegistry("https://${_host}", _credencial) {
                        app.push("${_BUILD_NUMBER}")
                        app.push("latest")
                    }

                    stash includes: '**', name: 'app'
                }

                //pushImageToRepo("${appDiskRepoPrd}",cloudCredencialPrd, env.BUILD_NUMBER,'',cloudProvider)                
            }
        }
        stage('Deploy Kong') {
			when {
                expression { return ( (env.BRANCH_NAME.find("release") && userInput == true) || env.BRANCH_NAME == "develop" )}
            }
			steps {
               	script {
                    echo 'Creating Kong Route...'
                    if(env.BRANCH_NAME == "develop"){
                        createKongRoute(env.BRANCH_NAME,env.JOB_NAME,requestMappingURI,namespacek8sDev,cloudProvider)   
                    } else if(env.BRANCH_NAME.find("release")){
                        createKongRoute(env.BRANCH_NAME,env.JOB_NAME,requestMappingURI,namespacek8sHml,cloudProvider)
                    }     
                }
            }
        }
    }    
    post {
        success {
            slackSend (channel: "#${slackChannel}",color: '#62A7E8', 
                message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
        }
        failure {
            slackSend (channel: "#${slackChannel}", color: '#E04422', 
                message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
            sendEmail(emailextrecipients([ [$class: 'CulpritsRecipientProvider'] ]),
                "build-failure", "[JENKINS - ERRO] Build Failure")    
        }
    }     
}
