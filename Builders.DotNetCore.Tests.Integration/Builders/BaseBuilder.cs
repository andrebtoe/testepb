﻿namespace Builders.DotNetCore.Tests.Integration.Builders
{
    public abstract class BaseBuilder<T>
    {
        protected T Built;

        public BaseBuilder(T built)
        {
            Built = built;
        }

        public virtual T Get()
        {
            return Built;
        }
    }
}
