﻿using Builders.DotNetCore.Infrastructure;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;

namespace Builders.DotNetCore.Data.Mongo
{
    public class MongoUnitOfWork : IUnitOfWork
    {
        private readonly IMongoDatabase _database;
        private readonly MongoClient _client;

        public MongoUnitOfWork(IOptions<MongoSettings> settings)
        {
            _client = new MongoClient(settings.Value.ConnectioString);
            if (_client.IsNotNull())
            {
                _database = _client.GetDatabase(settings.Value.Database);
            }
        }

        public IMongoDatabase Database
        {
            get { return _database; }
        }

        public Repository<TEntity> GetRepository<TEntity>() where TEntity : class
        {
            return new MongoRepository<TEntity>(_database);
        }

        public void FlushSession()
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}