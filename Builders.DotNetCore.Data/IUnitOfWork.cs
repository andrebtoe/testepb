﻿using MongoDB.Driver;
using System;

namespace Builders.DotNetCore.Data
{
    public interface IUnitOfWork : IDisposable
    {
        Repository<TEntity> GetRepository<TEntity>() where TEntity : class;
        IMongoDatabase Database { get; }
        void FlushSession();
    }
}