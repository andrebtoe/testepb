﻿using Builders.DotNetCore.Domain.SharedContext.Commands;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;

namespace Builders.DotNetCore.WebApi.ActionsFilters
{
    public class ValidateCommandActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            if (context.HttpContext.Request.Method == "GET")
                return;

            if (context.ActionArguments.Count > 0)
            {
                var nameParameter = context.ActionArguments.First().Key;
                var command = context.ActionArguments[nameParameter] as ICommand;
                if (command != null)
                {
                    var validationResult = command.Validate();
                    if (!validationResult.IsValid)
                    {
                        context.Result = new BadRequestResult();
                        return;
                    }
                }
            }
        }
    }
}
