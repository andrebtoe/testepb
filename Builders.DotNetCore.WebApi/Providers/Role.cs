﻿using System;

namespace Builders.DotNetCore.WebApi.Providers
{
    public class Role
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
