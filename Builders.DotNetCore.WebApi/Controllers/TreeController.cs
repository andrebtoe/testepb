﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs;
using Builders.DotNetCore.Domain.SharedContext.Entities;
using Builders.DotNetCore.Domain.SharedContext.Handlers.TreeHandlers.Interfaces;
using Builders.DotNetCore.Domain.SharedContext.Outputs.TreeOutputs;
using Builders.DotNetCore.Domain.SharedContext.Repositories.TreeRepositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("Tree")]
    public class TreeController : Controller
    {
        private readonly ITreeHandler _treeHandler;
        private readonly ITreeRepository _treeRepository;

        public TreeController(ITreeHandler treeHandler, ITreeRepository treeRepository)
        {
            _treeHandler = treeHandler;
            _treeRepository = treeRepository;
        }

        [HttpGet]
        public IList<Tree> Get()
        {
            var tree = _treeRepository.GetAll();
            return tree;
        }

        [HttpPost]
        // Command validado automaticamente através do action filter: ValidateCommandActionFilter
        public TreeOutputResult Post([FromBody] CreateTreeCommand createTreeCommand)
        {
            var createCommandResult = _treeHandler.Handle(createTreeCommand);
            return createCommandResult.Result;
        }

        [HttpPatch]
        [Route("Tree/addChild")]
        public TreeOutputResult Patch([FromBody] UpdateTreeCommand updateTreeCommand)
        {
            var updateCommandResult = _treeHandler.Handle(updateTreeCommand);
            return updateCommandResult.Result;
        }
    }
}