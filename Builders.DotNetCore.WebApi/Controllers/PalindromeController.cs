﻿using Microsoft.AspNetCore.Mvc;

namespace Builders.DotNetCore.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("Palindrome")]
    public class PalindromeController : Controller
    {
        [Route("/palindrome/isPalindrome")]
        public IActionResult Get(string word)
        {
            return BadRequest();
            return Json(new PalindromeViewModel() { Word = "bla", IsPalindrome = true });
        }
    }

    public class PalindromeViewModel
    {
        public string Word { get; set; }
        public bool IsPalindrome { get; set; }
    }
}