﻿using System.ServiceModel;

namespace Builders.DotNetCore.WebApi.Soap
{
    [ServiceContract]
    public interface IPingService
    {
        [OperationContract]
        string Ping(string msg);
    }
}
