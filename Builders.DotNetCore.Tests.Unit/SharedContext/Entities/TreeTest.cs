﻿using Builders.DotNetCore.Domain.SharedContext.Entities;
using System;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit.SharedContext.Entities
{
    public class TreeTest
    {
        [Fact]
        public void Test_Specification_Valid()
        {
            // Arrange
            var tree = new Tree(int.MaxValue);
            var treeChild = new TreeChild(int.MaxValue);

            // Action
            tree.AddTreeChild(treeChild);
            var validationResult = tree.Validate();

            // Assert
            Assert.True(validationResult.IsValid == true, "Specification invalid");
        }

        [Fact]
        public void Test_Specification_Invalid_Case1()
        {
            // Arrange
            var tree = new Tree(int.MaxValue);

            // Action
            var validationResult = tree.Validate();

            // Assert
            Assert.True(validationResult.IsValid == false, "Specification invalid");
        }

        [Fact]
        public void Test_Specification_Invalid_Case2()
        {
            // Arrange
            var tree = new Tree();

            // Action
            var validationResult = tree.Validate();

            // Assert
            Assert.True(validationResult.IsValid == false, "Specification invalid");
        }

        [Fact]
        public void Test_AddTreeChild_Case1()
        {
            // Arrange
            var tree = new Tree(int.MaxValue);

            // Assert
            Assert.Throws<ArgumentException>(() => tree.AddTreeChild(null));
        }

        [Fact]
        public void Test_AddTreeChild_Case2()
        {
            // Arrange
            var tree = new Tree();
            var treeChild = new TreeChild();
            var times = new Random().Next(10);

            // Action
            for (int i = 0; i < times; i++)
                tree.AddTreeChild(treeChild);

            // Assert
            Assert.True(tree.Children.Count == times, "Specification invalid");
        }
    }
}