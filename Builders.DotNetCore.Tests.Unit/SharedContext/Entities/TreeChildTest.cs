﻿using Builders.DotNetCore.Domain.SharedContext.Entities;
using System;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit.SharedContext.Entities
{
    public class TreeChildTest
    {
        [Fact]
        public void Test_Specification_Valid()
        {
            // Arrange
            var treeChild = new TreeChild(int.MaxValue);
            var treeChild2 = new TreeChild(int.MaxValue);

            // Action
            treeChild.AddTreeChild(treeChild2);
            var validationResult = treeChild.Validate();

            // Assert
            Assert.True(validationResult.IsValid == true, "Specification invalid");
        }

        [Fact]
        public void Test_Specification_Invalid_Case1()
        {
            // Arrange
            var treeChild = new TreeChild(int.MaxValue);

            // Action
            var validationResult = treeChild.Validate();

            // Assert
            Assert.True(validationResult.IsValid == false, "Specification invalid");
        }

        [Fact]
        public void Test_Specification_Invalid_Case2()
        {
            // Arrange
            var treeChild = new TreeChild();

            // Action
            var validationResult = treeChild.Validate();

            // Assert
            Assert.True(validationResult.IsValid == false, "Specification invalid");
        }

        [Fact]
        public void Test_AddTreeChild_Case1()
        {
            // Arrange
            var treeChild = new TreeChild(int.MaxValue);

            // Assert
            Assert.Throws<ArgumentException>(() => treeChild.AddTreeChild(null));
        }

        [Fact]
        public void Test_AddTreeChild_Case2()
        {
            // Arrange
            var treeChild = new TreeChild();
            var treeChild2 = new TreeChild();
            var times = new Random().Next(10);

            // Action
            for (int i = 0; i < times; i++)
                treeChild.AddTreeChild(treeChild2);

            // Assert
            Assert.True(treeChild.Children.Count == times, "Specification invalid");
        }
    }
}