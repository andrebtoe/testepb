﻿using Builders.DotNetCore.Domain.SharedContext.Commands.TreeCommands.Inputs;
using Builders.DotNetCore.Domain.SharedContext.Entities;
using Builders.DotNetCore.Domain.SharedContext.Handlers.TreeHandlers;
using Builders.DotNetCore.Domain.SharedContext.Repositories.TreeRepositories;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Builders.DotNetCore.Tests.Unit.SharedContext.Handlers.TreeHandlers
{
    public class TreeHandlerTest
    {
        [Fact]
        public void Test_Method_FillChildrenByReference()
        {
            // Arrange
            var mockTreeRepository = new Mock<ITreeRepository>();
            var treeHandler = new TreeHandler(mockTreeRepository.Object);
            var tree = new Tree(10);
            var children = new List<CreateTreeChildCommand>() {
                new CreateTreeChildCommand(){
                    Children = new List<CreateTreeChildCommand>(){
                        new CreateTreeChildCommand(){
                            Children = new List<CreateTreeChildCommand>()
                        }
                    }
                }
            };

            // Action
            treeHandler.FillChildrenByReference(children, tree, null);

            // Assert
            Assert.True(tree.Children.Count == 1, "Specification invalid");
        }

        [Fact]
        public void Test_Method_Create_Handler()
        {
            // Arrange
            var mockTreeRepository = new Mock<ITreeRepository>();
            var treeHandler = new TreeHandler(mockTreeRepository.Object);
            var valueRoot = 100;
            var valueSub = 10;
            var createTreeCommand = new CreateTreeCommand()
            {
                Value = valueRoot,
                Children = new List<CreateTreeChildCommand>() {
                    new CreateTreeChildCommand(){
                        Value = valueSub
                    }
                }
            };

            // Setup
            mockTreeRepository.Setup(x => x.Add(It.IsAny<Tree>())).Returns(true);

            // Action
            var result = treeHandler.Handle(createTreeCommand);

            // Assert
            Assert.True(result.Result.Value == valueRoot, "Specification invalid");
            Assert.True(result.Result.Children.Single().Value == valueSub, "Specification invalid");
        }

        [Fact]
        public void Test_Method_Update_Handler()
        {
            // Arrange
            var mockTreeRepository = new Mock<ITreeRepository>();
            var treeHandler = new TreeHandler(mockTreeRepository.Object);
            var valueRoot = 100;
            var valueSub = 10;
            var tree = new Tree()
            {
                Value = 150,
                Children = new List<TreeChild>() {
                    new TreeChild(){
                        Value = 189
                    }
                }
            };
            var updateTreeCommand = new UpdateTreeCommand()
            {
                TreeId = "",
                Value = valueRoot,
                Children = new List<CreateTreeChildCommand>() {
                    new CreateTreeChildCommand(){
                        Value = valueSub
                    }
                }
            };

            // Setup
            mockTreeRepository.Setup(x => x.GetByTreeId(It.IsAny<string>())).Returns(tree);

            // Action
            var result = treeHandler.Handle(updateTreeCommand);

            // Assert
            Assert.True(result.Result.Value == 150, "Specification invalid");
        }
    }
}